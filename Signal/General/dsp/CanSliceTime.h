//-*-C++-*-
/***************************************************************************
 *
 *   Copyright (C) 2024 by Willem van Straten
 *   Licensed under the Academic Free License version 2.1
 *
 ***************************************************************************/

// dspsr/Signal/General/dsp/CanSliceTime.h

#ifndef __dspsr_CanSliceTime_h
#define __dspsr_CanSliceTime_h

#include "MJD.h"

namespace dsp {

  class Source;

  //! Interface to things that can slice in time
  class CanSliceTime
  {
  public:

    //! Set the start of the time slice
    virtual void set_start_time (const MJD&) = 0;

    //! Set the end of the time slice
    virtual void set_end_time (const MJD&) = 0;
  };
}

#endif // !defined(__CanSliceTime_h)





