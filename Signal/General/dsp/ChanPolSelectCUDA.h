//-*-C++-*-

/***************************************************************************
 *
 *   Copyright (C) 2024 by Willem van Straten
 *   Licensed under the Academic Free License version 2.1
 *
 ***************************************************************************/

#ifndef __dsp_ChanPolSelectEngine_h
#define __dsp_ChanPolSelectEngine_h

#include "dsp/ChanPolSelect.h"
#include "dsp/LaunchConfig.h"

namespace CUDA
{
  //! Performs the ChanPolSelect transformation on a GPU using CUDA
  class ChanPolSelectEngine : public dsp::ChanPolSelect::Engine
  {
  public:

    //! Construct with optional stream
    ChanPolSelectEngine (cudaStream_t stream = 0);

    //! Perform any internal setup
    void setup (dsp::ChanPolSelect* user) override;

    //! Copies the selected frequency channels and polarizations for FPT or TFP ordered data
    void select (const dsp::TimeSeries* in, dsp::TimeSeries* out) override;

  protected:

    //! CUDA stream in which this transformation will operate
    cudaStream_t stream;

    //! gpu configuration
    LaunchConfig gpu_config;
  };
}

#endif // !defined(__dsp_ChanPolSelectEngine_h)
