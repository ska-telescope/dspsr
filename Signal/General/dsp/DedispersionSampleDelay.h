//-*-C++-*-
/***************************************************************************
 *
 *   Copyright (C) 2007 by Willem van Straten
 *   Licensed under the Academic Free License version 2.1
 *
 ***************************************************************************/

// dspsr/Signal/General/dsp/DedispersionSampleDelay.h

#ifndef __Dedispersion_SampleDelay_h
#define __Dedispersion_SampleDelay_h

#include "dsp/Dedispersion.h"
#include "dsp/SampleDelayFunction.h"

namespace dsp {

  class Dedispersion::SampleDelay : public SampleDelayFunction {
    
  public:
    
    //! Default constructor
    SampleDelay ();

    //! Compute the dispersive delays for the specified Observation
    /*! \param obs the Observation to match
        \retval true if the function has changed */
    bool match (const Observation* obs);
    
    //! Return the integer sample dispersive delay for the given frequency channel
    int64_t get_delay (unsigned ichan, unsigned ipol=0) const;
    
    //! Return the residual fractional sample dispersive delay for the given frequency channel
    double get_fractional_delay (unsigned ichan, unsigned ipol=0) const;

    //! Return the deispersive delay for the centre of the frequency channel range
    int64_t get_delay_range (unsigned schan, unsigned echan, unsigned ipol) const;

    //! Add to the history of operations performed on the observation
    void mark (Observation* observation);

    //! Copy attributes from the specified Observation
    void init (const Observation* obs);

    //! Return the integer and fractional sample dispersive delay for the specified frequency (in MHz)
    virtual std::pair<int64_t, double> get_sample_delay (double frequency_MHz);

    //! Return the integer and fractional sample dispersive delays in seconds for the specified frequency
    std::pair<double, double> get_time_delay (double frequency_MHz);

  protected:
    
    //! Centre frequency of the band-limited signal in MHz
    double centre_frequency;
    
    //! Bandwidth of signal in MHz
    double bandwidth;
    
    //! Dispersion measure (in \f${\rm pc cm}^{-3}\f$)
    double dispersion_measure;
    
    //! The sampling rate (in Hz)
    double sampling_rate;
    
    //! The integer-sample dispersive delays
    std::vector<int64_t> delays;
    
    //! The residual fractional-sample dispersive delays
    std::vector<double> fractional_delays;
    
    //! The channel centre frequencies
    std::vector<double> freqs;
  };

}

#endif
