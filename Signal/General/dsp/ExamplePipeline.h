//-*-C++-*-
/***************************************************************************
 *
 *   Copyright (C) 2024 by Willem van Straten
 *   Licensed under the Academic Free License version 2.1
 *
 ***************************************************************************/

// dspsr/Signal/General/dsp/ExamplePipeline.h

#ifndef __dspsr_ExamplePipeline_h
#define __dspsr_ExamplePipeline_h

#include "dsp/SingleThread.h"

namespace dsp {

  //! A single ExamplePipeline thread
  class ExamplePipeline : public SingleThread
  {

  public:

    //! Create the pipeline
    void construct () override;

    //! Prepare before running
    void prepare () override;

  };

}

#endif // !defined(__ExamplePipeline_h)

