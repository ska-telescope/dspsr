//-*-C++-*-
/***************************************************************************
 *
 *   Copyright (C) 2015 by Matthew Kerr
 *   Licensed under the Academic Free License version 2.1
 *
 ***************************************************************************/

// cribbed from LoadToFil

#ifndef __dspsr_LoadToFITS_h
#define __dspsr_LoadToFITS_h

#include "dsp/SingleThread.h"
#include "dsp/TimeSeries.h"
#include "dsp/Filterbank.h"
#include "dsp/FilterbankConfig.h"
#include "dsp/Dedispersion.h"
#include "dsp/OutputFile.h"

namespace dsp {

  //! A single LoadToFITS thread
  class LoadToFITS : public SingleThread
  {

  public:

    //! Configuration parameters
    class Config;

    //! Set the configuration to be used in prepare and run
    void set_configuration (Config*);

    //! Constructor
    LoadToFITS (Config* config = 0);

    //! Create the pipeline
    void construct ();

    //! Final preparations before running
    void prepare ();

    //! Configuration parameters
    Reference::To<Config> config;

    //! The filterbank in use
    Reference::To<Filterbank> filterbank;

    //! The dedispersion kernel
    Reference::To<Dedispersion> kernel;

    //! The convolution operation
    Reference::To<Convolution> convolution;

    //! The output file
    Reference::To<OutputFile> outputFile;

    //! Verbose output
    static bool verbose;

    friend class LoadToFITSN;
  };

  //! Load, unpack, filterbank, re-digitize, and write to FITS
  class LoadToFITS::Config : public SingleThread::Config
  {
  public:

    // Sets default values
    Config ();

    // set block size to this factor times the minimum possible
    void set_times_minimum_ndat (unsigned);
    unsigned get_times_minimum_ndat () const { return times_minimum_ndat; }

    // set block_size to result in at least this much RAM usage
    void set_maximum_RAM (uint64_t);
    uint64_t get_maximum_RAM () const { return maximum_RAM; }

    // input data block size in MB
    double block_size = 2.0;

    // order in which the unpacker will output time samples
    dsp::TimeSeries::Order order = dsp::TimeSeries::OrderTFP;

    //! Filterbank config options
    Filterbank::Config filterbank;

    //! when unpacking FITS data, denormalize using DAT_SCL and DAT_OFFS
    bool apply_FITS_scale_and_offset = false;

    //! set block size to this factor times the minimum possible
    unsigned times_minimum_ndat = 1;

    //! Maximum RAM to use (per thread) in bytes
    double maximum_RAM = 256 * 1024 * 1024;

    //! dispersion measure set in output file
    double dispersion_measure = 0.0;

    //! removed inter-channel dispersion delays
    bool dedisperse = false;

    //! coherently dedisperse along with filterbank
    bool coherent_dedispersion = false;

    //! phase-coherent Faraday rotation correction (not implemented)
    bool coherent_derotation = false;

    //! artificial delay to the start time in seconds
    double start_time_delay = 0.0;

    //! integrate in time before digitization
    unsigned tscrunch_factor = 0;

    //! integrate in frequency before digitization
    unsigned fscrunch_factor = 0;

    //! polarizations (Intensity, AABB, or Coherency)
    int npol = 4;

    //! time interval (in seconds) between offset and scale updates
    double rescale_seconds = -1.0;

    //! hold offset and scale constant after first update
    bool rescale_constant = false;

    //! set maximum length for a file
    double integration_length = 0;
    
    //! number of bits used to re-digitize the floating point time series
    int nbits = 2;

    //! integration time per output sample (in seconds)
    double tsamp = 64e-6;

    //! number of samples per output data block
    unsigned nsblk = 2048;

    //! Name of the output file
    std::string output_filename;

    //! produce upper sideband output
    bool upper_sideband_output = false;

    //! Set quiet mode
    virtual void set_quiet ();

    //! Set verbose
    virtual void set_verbose();

    //! Set very verbose
    virtual void set_very_verbose();

  };
}

#endif // !defined(__LoadToFITS_h)





