//-*-C++-*-
/***************************************************************************
 *
 *   Copyright (C) 2011 by Willem van Straten
 *   Licensed under the Academic Free License version 2.1
 *
 ***************************************************************************/

// dspsr/Signal/General/dsp/LoadToSlice.h

#ifndef __dspsr_LoadToSlice_h
#define __dspsr_LoadToSlice_h

#include "dsp/SingleThread.h"
#include "dsp/CanSliceTime.h"
#include "dsp/DedispersionPipeConfig.h"
#include "dsp/OutputFile.h"
#include "dsp/Truncate.h"

namespace dsp {

  //! A single LoadToSlice thread
  class LoadToSlice : public SingleThread, public DedispersionPipe, public CanSliceTime
  {

  public:

    //! Configuration parameters
    class Config;

    //! Set the configuration to be used in prepare and run
    void set_configuration (Config*);

    //! Constructor
    LoadToSlice (Config* config = 0);

    //! Create the pipeline
    void construct () override;

    //! Prepare before run
    void prepare () override;

    //! Finish after run
    void finish() override;

    //! Set the start of the time slice
    void set_start_time (const MJD&) override;

    //! Set the end of the time slice
    void set_end_time (const MJD&) override;

  private:

    friend class LoadToSliceN;

    //! Configuration parameters
    Reference::To<Config> config;

    //! The end of data
    Reference::To<Truncate> truncate;

    //! The output file
    Reference::To<OutputFile> outputFile;

    //! Verbose output
    static bool verbose;
  };

  //! Load, unpack, process and fold data into phase-averaged profile(s)
  class LoadToSlice::Config : public SingleThread::Config
  {
  public:

    // Sets default values
    Config () = default;

    //! Add command line options
    void add_options (CommandLine::Menu&) override;

    //! Dededispersion configuration options
    DedispersionPipe::Config dedisp;

    //! Configuration of 2-bit correction options
    TwoBitCorrection::Config twobit_config;

    //! when unpacking FITS data, denormalize using DAT_SCL and DAT_OFFS
    bool apply_FITS_scale_and_offset = false;

    //! Name of the output file
    std::string output_filename;
  };
}

#endif // !defined(__LoadToSlice_h)





