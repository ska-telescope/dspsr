//-*-C++-*-
/***************************************************************************
 *
 *   Copyright (C) 2023 by Willem van Straten
 *   Licensed under the Academic Free License version 2.1
 *
 ***************************************************************************/

#ifndef __dsp_Signal_General_MaskTimes_h
#define __dsp_Signal_General_MaskTimes_h

#include "dsp/Mask.h"
#include "TimeInterval.h"

namespace dsp {

  //! Mask samples in the input TimeSeries that are spanned by time intervals
  /*! The time intervals are loaded from file and must be sorted in time order. */
  class MaskTimes: public Mask {

  public:

    //! Default constructor
    MaskTimes ();

    //! Destructor
    ~MaskTimes ();

    //! Load intervals to be masked from file
    void load (const std::string& filename);

    //! Get the number of intervals
    unsigned size() { return interval.size(); }

    //! Get the ith interval
    const TimeInterval& get_interval(unsigned i) { return interval.at(i); }

  protected:

    //! Mask any time samples spanned by an interval
    void mask_data();

    //! List of time intervals to be masked
    /*! Each <MJD,double> pair is the start time and duration (in seconds) of an interval to be masked. */
    std::vector<TimeInterval> interval;

    //! The current time interval
    unsigned current_interval{0};
  };

}

#endif // !defined(__dsp_Signal_General_MaskTimes_h)
