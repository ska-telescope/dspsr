//-*-C++-*-

/***************************************************************************
 *
 *   Copyright (C) 2015-2025 by Matthew Kerr and Andrew Jameson
 *   Licensed under the Academic Free License version 2.1
 *
 ***************************************************************************/

#ifndef __dsp_TScrunchEngine_h
#define __dsp_TScrunchEngine_h

#include "dsp/TScrunch.h"

#include <cuda_runtime.h>

namespace CUDA
{
  /**
   * @brief CUDA Engine for the TScrunch operation.
   *
   */
  class TScrunchEngine : public dsp::TScrunch::Engine
  {
  public:

    /**
     * @brief Construct a new TScrunchEngine object.
     *
     * @param stream CUDA stream into which GPU operations will be scheduled
     */
    TScrunchEngine (cudaStream_t stream);

    /**
     * @brief Perform a temporal scrunch by the sfactor from the input timeseries to the output
     * timeseries where both have FPT ordering.
     *
     * @param in input timeseries to read
     * @param out output timeseries to write, after tscrunching
     * @param sfactor number of input time samples to sum into each output time sample
     */
    void fpt_tscrunch (const dsp::TimeSeries * input,
                       dsp::TimeSeries * output,
                       unsigned sfactor);

    /**
     * @brief Perform a temporal scrunch by the sfactor from the input timeseries to the output
     * timeseries where both have TFP ordering.
     *
     * @param in input timeseries to read
     * @param out output timeseries to write, after tscrunching
     * @param sfactor number of input time samples to sum into each output time sample
     */
    void tfp_tscrunch (const dsp::TimeSeries * input,
                       dsp::TimeSeries * output,
                       unsigned sfactor);

  protected:

    //! CUDA stream into which GPU operations will be scheduled
    cudaStream_t stream;
  };

} // namespace dsp

#endif // !defined(__dsp_TScrunchEngine_h)
