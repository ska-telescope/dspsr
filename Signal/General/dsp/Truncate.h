//-*-C++-*-
/***************************************************************************
 *
 *   Copyright (C) 2008 by Willem van Straten
 *   Licensed under the Academic Free License version 2.1
 *
 ***************************************************************************/

// dspsr/Signal/General/dsp/Truncate.h


#ifndef __Truncate_h
#define __Truncate_h

#include "dsp/Transformation.h"
#include "dsp/TimeSeries.h"

namespace dsp {

  //! Truncates the input at a specified epoch and raises the EndOfFile exception
  class Truncate : public Transformation <TimeSeries, TimeSeries>
  {

  public:
    
    //! Constructor
    Truncate ();
    
    //! Set the epoch at which to truncate the input
    void set_end_time (const MJD& mjd) { end_epoch = mjd; }

  protected:

    //! Truncate the input data
    virtual void transformation ();

    //! Epoch at which data will be ended
    MJD end_epoch;
  };

}

#endif // !defined(__Truncate_h)
