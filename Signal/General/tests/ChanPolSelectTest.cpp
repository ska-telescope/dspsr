/***************************************************************************
 *
 *   Copyright (C) 2024-2025 by Jesmigel Cantos and Will Gauvin
 *   Licensed under the Academic Free License version 2.1
 *
 ***************************************************************************/

#include "dsp/ChanPolSelectTest.h"
#include "dsp/TFPOffset.h"
#include "dsp/TimeSeries.h"
#include "dsp/WeightedTimeSeries.h"
#include "dsp/GtestMain.h"
#include "dsp/Memory.h"
#include "dsp/SignalStateTestHelper.h"

#include <algorithm>
#include <random>
#include <cassert>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_CUDA
#include "dsp/ChanPolSelectCUDA.h"
#include "dsp/TransferCUDATestHelper.h"
#include <cuda.h>
#endif

//! main method passed to googletest
int main(int argc, char* argv[])
{
  return dsp::test::gtest_main(argc, argv);
}

namespace dsp::test {

ChanPolSelectTest::ChanPolSelectTest()
{
  states = { Signal::Nyquist, Signal::Analytic, Signal::Intensity, Signal::PPQQ, Signal::Coherence };

#ifdef HAVE_CUDA
  /* CUDA-specific resources are constructed even if unused because
     the constructor is called only once */
  device_input = new dsp::TimeSeries;
  device_output = new dsp::TimeSeries;
  device_memory = new CUDA::DeviceMemory;
  device_input->set_memory(device_memory);
  device_output->set_memory(device_memory);
#endif
}

void ChanPolSelectTest::init_containers()
{
  if (use_wts)
  {
    input = input_wts = new dsp::WeightedTimeSeries;
    output = output_wts = new dsp::WeightedTimeSeries;
  }
  else
  {
    input = new dsp::TimeSeries;
    output = new dsp::TimeSeries;
    input_wts = output_wts = nullptr;
  }
}

void ChanPolSelectTest::SetUp()
{
}

void ChanPolSelectTest::TearDown()
{
  input = nullptr;
  input_wts = nullptr;
  output = nullptr;
  output_wts = nullptr;
}

// Function to generate a random number within a range [min, max]
int ChanPolSelectTest::generateRandomNumber(int min, int max)
{
  // Seed the random number generator
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<int> distribution(min, max);

  // Generate and return the random number
  return distribution(gen);
}

void ChanPolSelectTest::generate_data()
{
  if (order == dsp::TimeSeries::OrderFPT)
    generate_fpt();
  else
    generate_tfp();
}

void ChanPolSelectTest::generate_wts()
{
  input_wts->set_ndat_per_weight(ndat_per_weight);
  input_wts->resize(ndat);
  if (dsp::Observation::verbose) {
    std::cerr << "input_wts->get_ndat_per_weight(): " << input_wts->get_ndat_per_weight() << std::endl;
    std::cerr << "input_wts->get_npol_weight(): " << input_wts->get_npol_weight() << std::endl;
    std::cerr << "input_wts->get_nchan_weight(): " << input_wts->get_nchan_weight() << std::endl;
  }

  for (unsigned ichan=0; ichan<input_wts->get_nchan_weight(); ichan++)
    {
      for (unsigned ipol=0; ipol<input_wts->get_npol_weight(); ipol++)
      {
        uint16_t* ptr = input_wts->get_weights(ichan, ipol);
        assert(ptr != nullptr);

        uint64_t ival = 0;
        for (uint64_t iweight=0; iweight<input_wts->get_nweights(); iweight++)
        {
          if (iweight % 2)
          {
            ptr[ival] = uint16_t(ipol);
          }
          else
          {
            ptr[ival] = uint16_t(ichan);
          }
          ival++;
        }
      }
    }
}

void ChanPolSelectTest::generate_fpt()
{
  input->set_order(dsp::TimeSeries::OrderFPT);
  output->set_order(dsp::TimeSeries::OrderFPT);
  if (use_wts)
  {
    if (dsp::Observation::verbose)
    {
      std::cerr<<"input order: "<<input->get_order()<<std::endl;
      std::cerr<<"input_wts order: "<<input_wts->get_order()<<std::endl;
    }
    generate_wts();
  }
  else
  {
    input->resize(ndat);
  }

  for (unsigned ichan=0; ichan<input->get_nchan(); ichan++)
  {
    for (unsigned ipol=0; ipol<input->get_npol(); ipol++)
    {
      float* ptr = input->get_datptr(ichan, ipol);
      assert(ptr != nullptr);

      uint64_t ival = 0;
      for (uint64_t idat=0; idat<input->get_ndat(); idat++)
      {
        for (unsigned idim=0; idim<input->get_ndim(); idim++)
        {
          if (idat % 2)
          {
            ptr[ival] = float(ipol);
          }
          else
          {
            ptr[ival] = float(ichan);
          }
          ival++;
        }
      }
    }
  }
}

void ChanPolSelectTest::assert_data(bool nchan_weight_equals_nchan, bool npol_weight_equals_npol)
{
#ifdef HAVE_CUDA
  if (on_gpu)
  {
    output->zero();
    TransferCUDATestHelper xfer;
    xfer.copy(output, device_output, cudaMemcpyDeviceToHost);
  }
#endif

  if (dsp::Observation::verbose) {
    std::cerr << "ChanPolSelectTest::assert_data input nchan=" << input->get_nchan() << " npol=" << input->get_npol() << std::endl;
    std::cerr << "ChanPolSelectTest::assert_data output nchan=" << output->get_nchan() << " npol=" << output->get_npol() << std::endl;
    std::cerr << "ChanPolSelectTest::assert_data verifying dimensions of output" << std::endl;
  }

  ASSERT_EQ(number_of_channels_to_keep, output->get_nchan());
  ASSERT_EQ(number_of_polarizations_to_keep, output->get_npol());

  ASSERT_EQ(order, output->get_order());

  uint64_t ndat = output->get_ndat();
  ASSERT_EQ(input->get_ndat(), ndat);
  unsigned ndim = output->get_ndim();
  ASSERT_EQ(input->get_ndim(), ndim);

  if (use_wts)
  {
    assert_weights(nchan_weight_equals_nchan, npol_weight_equals_npol);
  }

  if (order == dsp::TimeSeries::OrderFPT)
    assert_fpt();
  else
    assert_tfp();

  if (dsp::Observation::verbose)
   std::cerr << "ChanPolSelectTest::assert_data all values are as expected" << std::endl;
}

void ChanPolSelectTest::assert_weights(bool nchan_weight_equals_nchan, bool npol_weight_equals_npol)
{
  uint64_t ndat = output_wts->get_ndat();
  unsigned ndim = output_wts->get_ndim();

  if (dsp::Observation::verbose)
    std::cerr << "ChanPolSelectTest::assert_weights ndat=" << ndat << " ndim=" << ndim << std::endl;

  unsigned error_count = 0;
  unsigned _start_channel_index = 0;
  unsigned _start_polarization_index = 0;

  if (nchan_weight_equals_nchan)
  {
    if (dsp::Observation::verbose)
      std::cerr << "ChanPolSelectTest::assert_weights nchan_weight==input_nchan" << std::endl;
    _start_channel_index = start_channel_index;
    ASSERT_EQ(number_of_channels_to_keep, output_wts->get_nchan_weight());
  }
  else
  {
    ASSERT_EQ(1, output_wts->get_nchan_weight());
  }

  if (npol_weight_equals_npol)
  {
    if (dsp::Observation::verbose)
      std::cerr << "ChanPolSelectTest::assert_weights npol_weight==input_npol" << std::endl;
    _start_polarization_index = start_polarization_index;
    ASSERT_EQ(number_of_polarizations_to_keep, output_wts->get_npol_weight());
  }
  else
  {
    ASSERT_EQ(1, output_wts->get_npol_weight());
  }

  // frequency
  for (unsigned ichan=0; ichan<input_wts->get_nchan_weight(); ichan++)
  {
    // polarization
    for (unsigned ipol=0; ipol<input_wts->get_npol_weight(); ipol++)
    {
      uint16_t* ptr = input_wts->get_weights(ichan, ipol);
      ASSERT_NE(ptr, nullptr);
      for (uint64_t iweight=0; iweight<input_wts->get_nweights(); iweight++)
      {
        uint16_t expected = 0;

        if (iweight % 2)
        {
          expected = ipol + _start_polarization_index;
        }
        else
        {
          expected = ichan + _start_channel_index;
        }
        ASSERT_EQ(expected, ptr[iweight]);
      }
    }
  }

  ASSERT_EQ(error_count,0);
}

void ChanPolSelectTest::assert_fpt()
{
  uint64_t ndat = output->get_ndat();
  unsigned ndim = output->get_ndim();

  if (dsp::Observation::verbose)
    std::cerr << "ChanPolSelectTest::assert_fpt ndat=" << ndat << " ndim=" << ndim << std::endl;

  unsigned error_count = 0;

  // frequency
  for (unsigned ichan=0; ichan<number_of_channels_to_keep; ichan++)
  {
    // polarization
    for (unsigned ipol=0; ipol<number_of_polarizations_to_keep; ipol++)
    {
      const float* ptr = output->get_datptr(ichan, ipol);
      uint64_t ival = 0;

      for (uint64_t idat=0; idat<ndat; idat++)
      {
        float expected = 0.0;

        if (idat % 2)
        {
          expected = ipol + start_polarization_index;
        }
        else
        {
          expected = ichan + start_channel_index;
        }

        for (unsigned idim=0; idim<ndim; idim++)
        {
          if (ptr[ival] != expected)
          {
            error_count ++;
            std::cerr << "assert_fpt FAIL ichan=" << ichan << " ipol=" << ipol << " idat=" << idat << " idim=" << idim
                      << " val=" << ptr[ival] << " != " << expected << std::endl;
          }

          ival++;
        }
      }
    }
  }

  ASSERT_EQ(error_count,0);
}

void ChanPolSelectTest::generate_tfp()
{
  input->set_order(dsp::TimeSeries::OrderTFP);
  output->set_order(dsp::TimeSeries::OrderTFP);

  if (use_wts)
  {
    generate_wts();
  }
  else
  {
    input->resize(ndat);
  }

  dsp::TFPOffset input_offset(input);

  float* ptr = input->get_dattfp();
  for (uint64_t idat=0; idat<ndat; idat++)
  {
    for (unsigned ichan=0; ichan<input->get_nchan(); ichan++)
    {
      for (unsigned ipol=0; ipol<input->get_npol(); ipol++)
      {
        auto ival = input_offset(idat, ichan, ipol);
        for (unsigned idim=0; idim<input->get_ndim(); idim++)
        {
          if (idat % 2)
          {
            ptr[ival] = float(ipol);
          }
          else
          {
            ptr[ival] = float(ichan);
          }

          ival ++;
        }
      }
    }
  }
}

void ChanPolSelectTest::assert_tfp()
{
  ASSERT_EQ(dsp::TimeSeries::OrderTFP, output->get_order());

  TFPOffset output_offset(output);

  uint64_t ndat = output->get_ndat();
  unsigned ndim = output->get_ndim();

  if (dsp::Observation::verbose)
    std::cerr << "ChanPolSelectTest::assert_tfp ndat=" << ndat << " ndim=" << ndim << std::endl;

  unsigned error_count = 0;

  const float* ptr = output->get_dattfp();

  // time
  for (uint64_t idat=0; idat<ndat; idat++)
  {
    // frequency
    for (unsigned ichan=0; ichan<number_of_channels_to_keep; ichan++)
    {
      // polarization
      for (unsigned ipol=0; ipol<number_of_polarizations_to_keep; ipol++)
      {
        float expected = 0.0;

        if (idat % 2)
        {
          expected = ipol + start_polarization_index;
        }
        else
        {
          expected = ichan + start_channel_index;
        }

        auto ival = output_offset(idat, ichan, ipol);

        for (unsigned idim=0; idim<ndim; idim++)
        {
          if (ptr[ival] != expected)
          {
            error_count ++;
            std::cerr << "assert_tfp FAIL idat=" << idat << " ichan=" << ichan << " ipol=" << ipol << " idim=" << idim
                      << " val=" << ptr[ival] << " != " << expected << std::endl;
          }

          ival++;
        }
      }
    }
  }

  ASSERT_EQ(error_count,0);
}

void ChanPolSelectTest::assert_transform_configurations(dsp::ChanPolSelect* cps)
{
  ASSERT_EQ(start_channel_index, cps->get_start_channel_index());
  ASSERT_EQ(number_of_channels_to_keep, cps->get_number_of_channels_to_keep());
  ASSERT_EQ(start_polarization_index, cps->get_start_polarization_index());
  ASSERT_EQ(number_of_polarizations_to_keep, cps->get_number_of_polarizations_to_keep());
}

void ChanPolSelectTest::test_driver(bool random_start_index, bool nchan_weight_equals_nchan, bool npol_weight_equals_npol)
{
  std::cerr << "ChanPolSelectTest::test_driver"
            << " random_start_index=" << random_start_index
            << " nchan_weight_equals_nchan=" << nchan_weight_equals_nchan
            << " npol_weight_equals_npol=" << npol_weight_equals_npol
            << std::endl;

  for (auto & state : states)
  {
    // iterate through the possible number of polarisations for the polarisation state
    std::vector<unsigned> npols = get_npols_for_state(state);
    for (auto & npol : npols)
    {
      unsigned ndim = get_ndim_for_state(state);

      if (dsp::Observation::verbose)
        std::cerr << "ChanPolSelectTest::test_driver"
                  << " state=" << state_string(state)
                  << " npol=" << npol
                  << std::endl;

      input->set_state(state);
      input->set_npol(npol);
      input->set_ndim(ndim);
      input->set_nchan(nchan);

      if (use_wts)
      {
        if (nchan_weight_equals_nchan)
        {
          input_wts->set_nchan_weight(input->get_nchan());
        }
        if (npol_weight_equals_npol)
        {
          input_wts->set_npol_weight(input->get_npol());
        }
      }
      if (dsp::Observation::verbose)
        std::cerr << "ChanPolSelectTest::test_driver generate_data" << std::endl;
      generate_data();

      for (unsigned nchan_keep=1;nchan_keep<nchan;nchan_keep++)
      {
        number_of_channels_to_keep = nchan_keep;
        number_of_polarizations_to_keep = 1;
        if (random_start_index)
        {
          start_channel_index = generateRandomNumber(0,number_of_channels_to_keep-1);
        }
        else
        {
          start_channel_index = 0;
        }

        start_polarization_index = 0;

        Reference::To<dsp::ChanPolSelect> cps(new_device_under_test());
        if (dsp::Observation::verbose)
          std::cerr << "ChanPolSelectTest::test_driver configure dsp::ChanPolSelect" << std::endl;
        EXPECT_NO_THROW(cps->set_number_of_channels_to_keep(number_of_channels_to_keep));
        EXPECT_NO_THROW(cps->set_start_channel_index(start_channel_index));
        EXPECT_NO_THROW(cps->set_number_of_polarizations_to_keep(number_of_polarizations_to_keep));
        assert_transform_configurations(cps);

        if (dsp::Observation::verbose)
          std::cerr << "ChanPolSelectTest::test_driver perform dsp::ChanPolSelect transform" << std::endl;

        if (random_start_index)
        {
          if(number_of_channels_to_keep+start_channel_index<=nchan)
          {
            ASSERT_TRUE(perform_transform(cps));
            assert_data(nchan_weight_equals_nchan, npol_weight_equals_npol);
          }
          else
          {
            ASSERT_FALSE(perform_transform(cps));
          }
        }
        else
        {
          ASSERT_TRUE(perform_transform(cps));
          if (dsp::Observation::verbose)
            std::cerr << "ChanPolSelectTest::test_driver verify dsp::ChanPolSelect output" << std::endl;
          assert_data(nchan_weight_equals_nchan, npol_weight_equals_npol);
        }

        if (dsp::Observation::verbose)
	  std::cerr << "ChanPolSelectTest::test_driver destroy dsp::ChanPolSelect" << std::endl;
        cps = nullptr;
      }
    }
  }
}

bool ChanPolSelectTest::perform_transform(dsp::ChanPolSelect* cps)
try
{
  cps->set_input(input);
  cps->set_output(output);

#ifdef HAVE_CUDA
  if (on_gpu)
  {
    TransferCUDATestHelper xfer;
    xfer.copy(device_input, input, cudaMemcpyHostToDevice);
    cps->set_input(device_input);
    cps->set_output(device_output);
  }
#endif

  cps->prepare();
  cps->operate();
  return true;
}
catch (std::exception &exc) {
  std::cerr << "Exception Caught: " << exc.what() << std::endl;
  return false;
}
catch (Error &error)
{
  std::cerr << "Error Caught: " << error << std::endl;
  return false;
}

dsp::ChanPolSelect* ChanPolSelectTest::new_device_under_test()
{
  Reference::To<dsp::ChanPolSelect> device = new dsp::ChanPolSelect;
#ifdef HAVE_CUDA
  if (on_gpu)
  {
    device->set_engine(new CUDA::ChanPolSelectEngine);
  }
#endif
  return device.release();
}

TEST_P(ChanPolSelectTest, test_construct_delete) // NOLINT
{
  Reference::To<dsp::ChanPolSelect> csp(new_device_under_test());
  ASSERT_NE(csp, nullptr);
  csp = nullptr;
  ASSERT_FALSE(csp);
}

TEST_P(ChanPolSelectTest, test_nchan_index_zero) // NOLINT
{
  auto param = GetParam();
  on_gpu = std::get<0>(param);
  order = std::get<1>(param);
  use_wts = std::get<2>(param);

  init_containers();
  // iterate through the possible polarisation states
  test_driver(false, false, false);
  if (dsp::Observation::verbose)
    std::cerr << "ChanPolSelectTest::test_nchan_index_zero exit" << std::endl;
}

TEST_P(ChanPolSelectTest, test_nchan_weight_equals_nchan) // NOLINT
{
  auto param = GetParam();
  on_gpu = std::get<0>(param);
  order = std::get<1>(param);
  use_wts = std::get<2>(param);

  init_containers();
  // iterate through the possible polarisation states
  test_driver(false, true, false);
  if (dsp::Observation::verbose)
    std::cerr << "ChanPolSelectTest::test_nchan_weight_equals_nchan exit" << std::endl;
}

TEST_P(ChanPolSelectTest, test_npol_weight_equals_npol) // NOLINT
{
  auto param = GetParam();
  on_gpu = std::get<0>(param);
  order = std::get<1>(param);
  use_wts = std::get<2>(param);

  init_containers();
  // iterate through the possible polarisation states
  test_driver(false, false, true);
  if (dsp::Observation::verbose)
    std::cerr << "ChanPolSelectTest::test_npol_weight_equals_npol exit" << std::endl;
}

TEST_P(ChanPolSelectTest, test_nchan_index_random) // NOLINT
{
  auto param = GetParam();
  on_gpu = std::get<0>(param);
  order = std::get<1>(param);
  use_wts = std::get<2>(param);

  init_containers();
  test_driver(true, false, false);
}

TEST_P(ChanPolSelectTest, test_nchan_induce_error) // NOLINT
{
  auto param = GetParam();
  on_gpu = std::get<0>(param);
  order = std::get<1>(param);
  use_wts = std::get<2>(param);

  init_containers();

  // iterate through the possible polarisation states
  for (auto & state : states)
  {
    // iterate through the possible number of polarisations for the polarisation state
    std::vector<unsigned> npols = get_npols_for_state(state);
    for (auto & npol : npols)
    {
      unsigned ndim = get_ndim_for_state(state);

      if (dsp::Observation::verbose)
        std::cerr << "ChanPolSelectTest::test_nchan_induce_error"
        << " state=" << state_string(state)
        << " npol=" << npol
        << " ndim=" << ndim
        << std::endl;

      input->set_state(state);
      input->set_npol(npol);
      input->set_ndim(ndim);
      input->set_nchan(nchan);

      Reference::To<dsp::ChanPolSelect> cps (new_device_under_test());
      generate_data();
      EXPECT_NO_THROW(cps->set_number_of_channels_to_keep(number_of_channels_to_keep));
      EXPECT_NO_THROW(cps->set_start_channel_index(start_channel_index));
      EXPECT_NO_THROW(cps->set_number_of_polarizations_to_keep(number_of_polarizations_to_keep));
      EXPECT_NO_THROW(cps->set_start_polarization_index(start_polarization_index));
      ASSERT_FALSE(perform_transform(cps));
      cps = nullptr;
    }
  }
}

std::vector<bool> get_gpu_flags()
{
#ifdef HAVE_CUDA
  int deviceCount;
  cudaError_t cudaStatus = cudaGetDeviceCount(&deviceCount);

  if (cudaStatus == cudaSuccess && deviceCount > 0)
  {
    return { false, true };
  }

  std::cout << "No GPU detected ... tests of CUDA::ChanPolSelectEngine disabled" << std::endl;
#endif
  return { false };
}

  std::vector<std::tuple<bool, dsp::TimeSeries::Order, bool>> get_test_parameters() {
    std::vector<std::tuple<bool, dsp::TimeSeries::Order, bool>> params{};

    for (auto on_gpu: get_gpu_flags())
    {
      if (on_gpu)
      {
        // currently don't handle weighted time series on GPU
        params.push_back(std::make_tuple(on_gpu, dsp::TimeSeries::OrderTFP, false));
        params.push_back(std::make_tuple(on_gpu, dsp::TimeSeries::OrderFPT, false));
      }
      else
      {
        // For TFP the weights are always in FPT order so we can perform ChanPolSelect for TFP ordered data
        params.push_back(std::make_tuple(on_gpu, dsp::TimeSeries::OrderTFP, false));
        params.push_back(std::make_tuple(on_gpu, dsp::TimeSeries::OrderTFP, true));
        // FPT can handle normal timeseries or a weighted time series
        params.push_back(std::make_tuple(on_gpu, dsp::TimeSeries::OrderFPT, false));
        params.push_back(std::make_tuple(on_gpu, dsp::TimeSeries::OrderFPT, true));
      }
    }

    return params;
  }

INSTANTIATE_TEST_SUITE_P(ChanPolSelectTestSuite, ChanPolSelectTest,
  testing::ValuesIn(get_test_parameters()),
  [](const testing::TestParamInfo<ChanPolSelectTest::ParamType>& info)
  {
    bool on_gpu = std::get<0>(info.param);
    auto order = std::get<1>(info.param);
    bool use_wts = std::get<2>(info.param);
    std::string name;
    if (on_gpu)
      name = "on_gpu";
    else
      name = "on_cpu";

    if (order == dsp::TimeSeries::OrderFPT)
      name += "_fpt";
    else
      name += "_tfp";

    if (use_wts)
      name += "_wts";
    return name;
  }
); // NOLINT

} // namespace dsp::test
